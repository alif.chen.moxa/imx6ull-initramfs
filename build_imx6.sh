#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0
#
# Authors:
# 	2018	Wes Huang	<Wes.Huang@moxa.com>

ROOT=$(pwd)
DEB_FOLDER="${ROOT}/deb"
BUSYBOX_FOLDER="${ROOT}/busybox-1.29.3"
BUSYBOX_CONFIG="moxa_tpm2_defconfig"

EXTERNAL_FOLDER="${ROOT}/initramfs"

BUILD_TIME=$(date +'20%y-%m-%d-%H:%M:%S')
OUTPUT_FOLDER="${ROOT}/output/${BUILD_TIME}"
ROOTFS_FOLDER="${OUTPUT_FOLDER}/rootfs"
DEB_ROOTFS_FOLDER="${OUTPUT_FOLDER}/deb_rootfs"
INITRD_FOLDER="${OUTPUT_FOLDER}/initrd"

build_busybox() {
	cd ${BUSYBOX_FOLDER}
	make ${BUSYBOX_CONFIG}
	make
	make install
	cd ${ROOT}
}

generate_deb_rootfs() {
	mkdir -p ${DEB_ROOTFS_FOLDER}
	mkdir -p ${ROOTFS_FOLDER}

	cd "${DEB_FOLDER}"

	for deb_file in $(ls);
	do
		ar -x ${deb_file}
		tar -xvf data.tar.xz -C ${DEB_ROOTFS_FOLDER}
	done

	rsync -av ${DEB_ROOTFS_FOLDER}/* ${ROOTFS_FOLDER}/
	cd ${ROOT}
}

rsync_external_to_rootfs() {
	#cp ../optee_apps/luks/host/optee_luks ${EXTERNAL_FOLDER}/usr/sbin/optee_luks
	#cp ../optee_apps/command/host/optee_command ${EXTERNAL_FOLDER}/usr/sbin/optee_command

	#arm-linux-gnueabihf-strip ${EXTERNAL_FOLDER}/usr/sbin/optee_luks
	#arm-linux-gnueabihf-strip ${EXTERNAL_FOLDER}/usr/sbin/optee_command

	rsync -av  ${EXTERNAL_FOLDER}/* ${ROOTFS_FOLDER}/
	#DEBUG
	#arm-linux-gnueabihf-strip ${ROOTFS_FOLDER}/sbin/cryptsetup.static
	#cp ../optee_apps/crucial/host/optee_crucial  ${ROOTFS_FOLDER}/usr/sbin/optee_crucial
	#cp ../optee_apps/luks/host/optee_luks ${ROOTFS_FOLDER}/usr/sbin/optee_luks
	#cp ../root-fs-optee/bin/tee-supplicant-static ${ROOTFS_FOLDER}/bin/tee-supplicant-static
        #cp ../optee_apps/command/host/optee_command ${ROOTFS_FOLDER}/usr/sbin/optee_command

        #arm-linux-gnueabihf-strip ${ROOTFS_FOLDER}/bin/tee-supplicant-static
	#arm-linux-gnueabihf-strip ${ROOTFS_FOLDER}/usr/sbin/optee_luks
	#arm-linux-gnueabihf-strip ${ROOTFS_FOLDER}/usr/sbin/optee_crucial
        #arm-linux-gnueabihf-strip ${ROOTFS_FOLDER}/usr/sbin/optee_command
}

create_rootfs_folder() {
	mkdir -p ${ROOTFS_FOLDER}
	cd ${ROOTFS_FOLDER}

	echo "necessary dir : bin sbin dev etc lib proc sys usr"
	echo "supported by busybox : bin sbin usr"
	echo "making dir : proc sys dev lib etc"
	mkdir -p proc sys dev lib etc
	echo "making dir : etc/init.d"
	mkdir -p etc/init.d
	echo "necessary dir : usr/bin usr/lib usr/sbin lib/modules"
	echo "supported by busybox : usr/bin usr/sbin"
	mkdir -p usr/lib lib/modules

	echo "making dir : mnt tmp var"
	mkdir -p mnt tmp var
	chmod 1777 tmp
	mkdir -p mnt/etc mnt/jffs2 mnt/yaffs mnt/data mnt/temp
	mkdir -p var/lib var/lock var/log var/run var/tmp
	chmod 1777 var/tmp
	echo "making dir : home root boot"
	mkdir -p home root boot
	mkdir -p overlayfs_rw overlayfs_ro

	mkdir -p lib/optee_armtz
	mkdir -p data
	mkdir -p data/tee
	echo "done"

	fakeroot mknod -m 640 dev/console c 5 1
	fakeroot mknod -m 664 dev/null    c 1 3


	rm -rf ${ROOT}/output/latest
	ln -sf ${OUTPUT_FOLDER} ${ROOT}/output/latest
	cd ${ROOT}
}

rsync_busybox_to_rootfs() {
	rsync -av ${BUSYBOX_FOLDER}/_install/* ${ROOTFS_FOLDER}/
}

generate_cpio_initrd() {
	cd ${ROOTFS_FOLDER}/
	mkdir -p ${INITRD_FOLDER}
	find . -print0 | cpio --null -ov --format=newc | gzip  > ${INITRD_FOLDER}/initrd.cpio.gz
	find . -print0 | cpio --null -ov --format=newc | xz -9 --format=lzma > ${INITRD_FOLDER}/initrd.cpio.xz

	mkimage -A arm -O linux -T ramdisk -d ${INITRD_FOLDER}/initrd.cpio.gz ${INITRD_FOLDER}/initrd.img
	mkimage -A arm -O linux -T ramdisk -d ${INITRD_FOLDER}/initrd.cpio.xz ${INITRD_FOLDER}/min_initrd.img

	cd ${ROOT}
}

show_info() {
	echo "Debian Package Root File System: ${DEB_ROOTFS_FOLDER}"
	echo "Root File System: ${ROOTFS_FOLDER}"
	echo "CPIO initrd file: ${INITRD_FOLDER}/initrd.cpio"
}

main() {
	build_busybox
	create_rootfs_folder
	rsync_busybox_to_rootfs
	rsync_external_to_rootfs
	generate_deb_rootfs
	generate_cpio_initrd
	show_info
}

main

exit 0
